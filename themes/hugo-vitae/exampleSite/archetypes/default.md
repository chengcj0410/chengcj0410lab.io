---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
description: "Guide to emoji usage in Hugo"
tags : [
    "Hugo"
]
draft: true
---

