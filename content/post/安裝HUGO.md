---
title: HUGO+Gitlab Pages製作靜態網頁"
date: 2021-06-08T23:06:07+08:00
draft: false
description: "Write something here"
tags : [
    "hugo","gitlab-pages"
]
Categories: "Gitlab"
---
# Gitlab+Hugo

對於有點程式背景的人總會想要動手做出屬於自己的小東西來嚐嚐鮮，偶然看到了Hugo後便開始找資料來試試看能不能架起來一個靜態的網頁。

此處使用的環境為windows ，如果選擇linux的話已經有許多神人的文章分享了!

## 1.安裝choco

使用管理員身分開啟powershell，並輸入以下指令。

```html
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

接著使用choco 安裝hugo，指令如下。

> choco install hugo -confirm

## 2.創建Gitlab pages

1.New project>>Create from template

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled.png)

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%201.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%201.png)

2.選擇Pages/Hugo模板來建置。

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%202.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%202.png)

3.Project 命名註冊

- Project name :填上gitlab帳號名.gitlab.io [註一]
- Visbility Level選擇Public

[註一]須符合上述命名規則才可正常使用gitlabPage的免費host服務。

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%203.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%203.png)

4.記下Clone的位址以便後續Push

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%204.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%204.png)

## 3.本地端資料夾創建

1.在希望的位址建置hugo資料夾，並將創建的gitlab-page clone到對應資料夾內。

這邊我將Hugo資料存放於 C:\Users\admin\Documents\Hugo  下，接著開啟powershell輸入以下指令。

> cd C:\Users\admin\Documents\Hugo

> git clone --recursive [https://gitlab.com/chengcj0410/chengcj0410.gitlab.io.git](https://gitlab.com/chengcj0410/chengcj0410.gitlab.io.git)

完成結果如下:

C:\Users\admin\Documents\Hugo\

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%205.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%205.png)

C:\Users\admin\Documents\Hugo\[chengcj0410.gitlab.io](http://chengcj0410.gitlab.io/)

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%206.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%206.png)

 2.下載Hugo theme

>到[HUGO](https://themes.gohugo.io/)官網上選擇喜歡的外觀主題，這邊我是使用[Vitae](https://themes.gohugo.io/hugo-vitae/)

> cd themes
git clone --recursive https://github.com/datacobra/hugo-vitae.git hugo-vitae

這邊要注意clone 的指令需加上 --recursive 否則會出現Pipeline失敗的問題，無法正常將網頁資料正常轉換為pages的格式。

錯誤訊息>>>fatal: No url found for submodule path theme/vitae in .gitmodules

3.本地端啟動Hugo server

開啟powershell輸入以下指令

> cd C:\Users\Admin\Documents\Hugo\[chengcj0410.gitlab.io](http://chengcj0410.gitlab.io/)

返回網頁資料夾跟目錄下。

> hugo server

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%207.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%207.png)

完成啟動server後，可在http://localhost:1313/下檢視網頁呈現樣式，並於按下Ctrl+C即可關閉server。

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%208.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%208.png)

## 4.Git上傳專案資料夾

1.確認網頁可正常呈現後，接著就可以將本地端的資料利用git push上gitlab page project了。

2.再次回到powershell輸入以下指令。

> git init

> git add .

> git commit -m "init commit"

> git remote add origin [https://gitlab.com/cheng](http://gitlab.com/cheng)cj0410/chengcj0410.gitlab.io.git

> git push -u origin master

此處可能需要輸入gitlab的帳號密碼，如果沒有申請SSH的話。

3.push 完成後即可回到gitlab查看是否成功完成，並確認 pipeline 是否完成delopy on呈現綠色勾勾。

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%209.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%209.png)

4.開啟gitlab page 完成的網址。

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%2010.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%2010.png)

![/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%2011.png](/Gitlab+Hugo%20d9ccd816a48243c6b2798b682b27d3fd/Untitled%2011.png)

gitlab project>chengcj0410.gitlab.io>settings>pages。

點擊Access pages下的http://chengcj0410.gitlab.io即可看到完成的blog!

## 5.Rreference

1. [**把手教你从0开始搭建自己的个人博客 |第二种姿势 | hugo**](https://www.youtube.com/watch?v=J7XQ2FeTHLg)
2. [Hugo quick start]()

[新增文章](https://www.notion.so/5448da52e45d436bb54587eb09e482ca)

